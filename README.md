# Due Date Calculator

App takes ticket open time and estimate of a processing hours and calculate processing deadline. It takes into account weekends and working shift. Main technologies: NodeJS, Express, Mocha, Sinon, Docker, Nginx

# Installation

1. `cd ./server`
2. `npm i`
3. `cd ../`
4. `sudo chmod +x dev.sh build-images.sh`
5. `./build-images.sh`

# Docker interactive mode start

1. `./dev.sh`

# Tests

1. `cd ./server`
2. `npm run test`

# Webserver & API

`Caution: submitDate takes ISO Date string at UTC timezone. Server timezone - UTC. Run server at docker to avoid using your local timezone as a server timezone.`

POST /v1/calculate HTTP/1.1
Host: localhost:80
Content-Type: application/json
Body: {
"submitDate": "YYYY-MM-DDTHH:MM:SS.000Z",
"turnaround": <integer>
}

Example: `{ "submitDate": "2019-10-08T16:00:00.000Z", "turnaround": 16 }`

# Conditions

We are looking for a solution that implements an issue tracking system’s due date
calculator. Your task is to implement the CalculateDueDate method, which takes the
submit date and turnaround time as an input and returns the date and time when the
issue is to be resolved.

These are the additional rules:

• Working hours are 9AM to 5PM every working day (Monday through Friday).
• The program should not deal with holidays, which means that a holiday on a
Thursday is still considered as a working day. A working Saturday will still be
considered as a nonworking day by the system.
• The turnaround time is given in working hours (for example 2 days equals to
16 hours). If a problem was reported at 2:12PM on Tuesday, the turnaround
time is 16 hours, then it is due by 2:12PM on Thursday.
• A problem can only be reported during working hours, which means that all
submit date values fall between 9AM and 5PM.
• You may not use any third-party libraries for date time calculations. (Eg.:
Moment.js, Carbon, Joda, etc.)
