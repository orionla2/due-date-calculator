#!/usr/bin/env bash
docker build -t ddc-nginx-proxy:0.0.1 --rm -f Dockerfile-proxy .
docker build -t ddc-server:0.0.1 --rm -f Dockerfile-server .