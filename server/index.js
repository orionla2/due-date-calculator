const express = require("express");
const bodyParser = require("body-parser");
const CalculatorRouter = require("./modules/calculator/calculator.router");
class Server {
  constructor(config) {
    this.app = config.app;
    this.router = new express.Router();
    this.prefix = "/v1";
    this.init();
    this.start();
  }

  static async build() {
    const app = express();
    return { app };
  }

  async init() {
    this.app.use(
      bodyParser.json({
        limit: "20mb"
      })
    );
    this.router.use("/calculate", CalculatorRouter.router);
    this.app.use(this.prefix, this.router);
  }

  start(port = 3000) {
    this.app.listen(port, err => {
      if (err) console.log("[Server::err]", err);
      console.log("[Server::Started on PORT]", port);
    });
  }
}

Server.build().then(config => {
  const server = new Server(config);
});
