const router = require("express").Router();
const calculatorController = require("./calculator.controller");
class CalculatorRouter {
  constructor() {
    this.router = router;
    this.controller = calculatorController;
    this.initRoutes();
  }

  initRoutes() {
    this.dueDateCalculator();
  }

  dueDateCalculator() {
    this.router.post(
      "/",
      this.dueDateCalculatorPayloadValidator,
      (req, res) => {
        try {
          const date = this.controller.dueDateCalculator(req.body);
          res.status(200).send(date);
        } catch (err) {
          res.status(400).send({ message: err.message });
        }
      }
    );
  }

  dueDateCalculatorPayloadValidator(req, res, next) {
    try {
      if (!req.body.turnaround || !req.body.submitDate)
        throw new Error("Required params turnaround & submitDate");
      if (
        typeof req.body.turnaround !== "number" ||
        !Number.isInteger(req.body.turnaround)
      )
        throw new Error("turnaround should be an integer");
      next();
    } catch (err) {
      res.status(400).send({ message: err.message });
    }
  }
}

module.exports = new CalculatorRouter();
