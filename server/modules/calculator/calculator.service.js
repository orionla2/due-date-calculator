class CalculatorService {
  constructor() {
    this.startHours = 9;
    this.endHours = 17;
    this.minutes = 0;
    this.seconds = 0;
    this.dayHours = 24;
    this.shiftLength =
      this.dayHours - this.startHours - (this.dayHours - this.endHours);
    this.nightTime = this.dayHours - this.endHours + this.startHours;
  }

  dueDateCalculator(submitDate, turnaround) {
    if (!this.checkIfValidDate(submitDate))
      throw new Error("Wrong submitDate.");
    if (!this.checkIfWorkingDay(submitDate))
      throw new Error("Sorry, we are open from MON to FRI");
    if (!this.submitAtCurrentBusinessDay(new Date().toISOString(), submitDate))
      throw new Error("Sorry, we are closed.");
    const shift = this.calculateBusinessDays(turnaround);
    const turnaroundTimestamp = this.calculateTurnaroundTimestamp(
      shift,
      submitDate
    );
    return {
      date: turnaroundTimestamp
    };
  }

  submitAtCurrentBusinessDay(now, date) {
    const startOfBusinessDay = new Date(now).setUTCHours(
      this.startHours,
      this.minutes,
      this.seconds
    );
    const endOfBusinessDay = new Date(now).setUTCHours(
      this.endHours,
      this.minutes,
      this.seconds
    );
    const comparisonDate = new Date(date).getTime();
    if (
      startOfBusinessDay < comparisonDate &&
      endOfBusinessDay > comparisonDate
    )
      return true;
    return false;
  }

  checkIfValidDate(date) {
    if (!isNaN(new Date(date))) return true;
    return false;
  }

  calculateBusinessDays(hours) {
    return {
      businessDays: Math.floor(hours / this.shiftLength),
      modulo: hours % this.shiftLength
    };
  }

  checkIfWorkingDay(timestamp) {
    const day = new Date(timestamp).getUTCDay();
    if (day === 0 || day === 6) return false;
    return true;
  }

  calculateTurnaroundTimestamp(shift, submitDate) {
    const turnaround = new Date(submitDate);
    const { modulo, businessDays } = shift;
    this.incrementModuloHours(turnaround, modulo);
    this.incrementBusinessDays(turnaround, businessDays);
    return turnaround;
  }

  incrementModuloHours(date, modulo) {
    while (modulo) {
      this.addHours(date);
      if (this.isOutOfWorkingShift(date)) this.addHours(date, this.nightTime);
      this.passHolidays(date);
      modulo--;
    }
  }

  incrementBusinessDays(date, businessDays) {
    while (businessDays) {
      this.addHours(date, this.dayHours);
      this.passHolidays(date);
      businessDays--;
    }
  }

  passHolidays(date) {
    if (!this.checkIfWorkingDay(date.toISOString())) {
      this.addHours(date, this.dayHours);
      this.passHolidays(date);
    }
  }

  addHours(date, hours = 1) {
    date.setTime(date.getTime() + hours * 60 * 60 * 1000);
  }

  isOutOfWorkingShift(date) {
    const compareHour = date.getUTCHours();
    if (this.startHours < compareHour && this.endHours > compareHour)
      return false;
    return true;
  }
}

module.exports = new CalculatorService();
