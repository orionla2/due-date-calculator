const calculatorService = require("./calculator.service");
class CalculatorController {
  constructor() {
    this.service = calculatorService;
  }

  dueDateCalculator(body) {
    const { submitDate, turnaround } = body;
    return this.service.dueDateCalculator(submitDate, turnaround);
  }
}

module.exports = new CalculatorController();
