const chai = require("chai");
const sinon = require("sinon");
const assert = chai.assert;
const calculatorController = require("../../modules/calculator/calculator.controller");
const calculatorService = require("../../modules/calculator/calculator.service");
const defaultSubmitDate = "2019-10-07T21:00:00.000Z";
const defaultTurnaround = 16;
describe("Calculator Controller", () => {
  it("Should be instance of CalculatorController", () => {
    const className = calculatorController.constructor.name;
    assert.equal(className, "CalculatorController");
  });

  it("Should have method `dueDateCalculator`", () => {
    assert.property(calculatorController, "dueDateCalculator");
  });

  it("`dueDateCalculator` should return object with Date", () => {
    const body = {
      submitDate: defaultSubmitDate,
      turnaround: defaultTurnaround
    };
    sinon
      .stub(calculatorService, "dueDateCalculator")
      .returns({ date: new Date() });
    const response = calculatorController.dueDateCalculator(body);
    assert.property(response, "date");
    assert.instanceOf(response.date, Date);
    sinon.restore();
  });
});
