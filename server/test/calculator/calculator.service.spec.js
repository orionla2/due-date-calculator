const chai = require("chai");
const sinon = require("sinon");
const assert = chai.assert;
const calculatorService = require("../../modules/calculator/calculator.service");
const defaultSubmitDate = "2019-10-07T21:00:00.000Z";
const defaultTurnaround = 16;
describe("Calculator Service", () => {
  it("Should be instance of CalculatorService", () => {
    const className = calculatorService.constructor.name;
    assert.equal(className, "CalculatorService");
  });

  it("Should have method `dueDateCalculator`", () => {
    assert.property(calculatorService, "dueDateCalculator");
  });

  it("Should work from MON to FRI and should not work at SUN & SAT", () => {
    const mon = "2019-10-07T12:00:00.000Z";
    const fri = "2019-10-11T12:00:00.000Z";
    const sat = "2019-10-12T12:00:00.000Z";
    const sun = "2019-10-13T12:00:00.000Z";
    const turnaround = 1;
    sinon.stub(calculatorService, "submitAtCurrentBusinessDay").returns(true);
    assert.doesNotThrow(() => {
      return calculatorService.dueDateCalculator(mon, turnaround);
    }, Error);
    assert.doesNotThrow(() => {
      return calculatorService.dueDateCalculator(fri, turnaround);
    }, Error);
    assert.throw(() => {
      return calculatorService.dueDateCalculator(sat, turnaround);
    }, "Sorry, we are open from MON to FRI");
    assert.throw(() => {
      return calculatorService.dueDateCalculator(sun, turnaround);
    }, "Sorry, we are open from MON to FRI");
    sinon.restore();
  });

  it("Should calculate turnaround at current day", () => {
    const stubShift = {
      modulo: 3,
      businessDays: 0
    };
    const submitDate = "2019-10-07T12:00:00.000Z";
    const expectedTimestamp = "2019-10-07T15:00:00.000Z";
    const resultDate = calculatorService.calculateTurnaroundTimestamp(
      stubShift,
      submitDate
    );
    assert.equal(resultDate.toISOString(), expectedTimestamp);
  });

  it("Should calculate turnaround at next working day at middle of a week", () => {
    const stubShift = {
      modulo: 3,
      businessDays: 0
    };
    const submitDate = "2019-10-07T15:20:00.000Z";
    const expectedTimestamp = "2019-10-08T10:20:00.000Z";
    const resultDate = calculatorService.calculateTurnaroundTimestamp(
      stubShift,
      submitDate
    );
    assert.equal(resultDate.toISOString(), expectedTimestamp);
  });

  it("Should calculate turnaround at next working day after weekend", () => {
    const stubShift = {
      modulo: 3,
      businessDays: 0
    };
    const submitDate = "2019-10-11T15:20:00.000Z";
    const expectedTimestamp = "2019-10-14T10:20:00.000Z";
    const resultDate = calculatorService.calculateTurnaroundTimestamp(
      stubShift,
      submitDate
    );
    assert.equal(resultDate.toISOString(), expectedTimestamp);
  });

  it("Should calculate turnaround at next working day after few business days at a middle of a week", () => {
    const stubShift = {
      modulo: 0,
      businessDays: 2
    };
    const submitDate = "2019-10-07T15:20:00.000Z";
    const expectedTimestamp = "2019-10-09T15:20:00.000Z";
    const resultDate = calculatorService.calculateTurnaroundTimestamp(
      stubShift,
      submitDate
    );
    assert.equal(resultDate.toISOString(), expectedTimestamp);
  });

  it("Should calculate turnaround at next working day after few business days at the end of a week", () => {
    const stubShift = {
      modulo: 0,
      businessDays: 2
    };
    const submitDate = "2019-10-10T15:20:00.000Z";
    const expectedTimestamp = "2019-10-14T15:20:00.000Z";
    const resultDate = calculatorService.calculateTurnaroundTimestamp(
      stubShift,
      submitDate
    );
    assert.equal(resultDate.toISOString(), expectedTimestamp);
  });

  it("Should calculate turnaround at worst case with mixed shift", () => {
    const stubShift = {
      modulo: 3,
      businessDays: 2
    };
    const submitDate = "2019-10-10T15:20:00.000Z";
    const expectedTimestamp = "2019-10-15T10:20:00.000Z";
    const resultDate = calculatorService.calculateTurnaroundTimestamp(
      stubShift,
      submitDate
    );
    assert.equal(resultDate.toISOString(), expectedTimestamp);
  });

  it("`dueDateCalculator` should return object with Date", () => {
    sinon.stub(calculatorService, "submitAtCurrentBusinessDay").returns(true);
    const response = calculatorService.dueDateCalculator(
      defaultSubmitDate,
      defaultTurnaround
    );
    assert.property(response, "date");
    assert.instanceOf(response.date, Date);
    sinon.restore();
  });

  it("Should properly validate date", () => {
    const validTimestamp = defaultSubmitDate;
    const validTimestampNumber = 10.1;
    const invalidTimestampString = "Invalid Timestamp";
    assert.isOk(calculatorService.checkIfValidDate(validTimestamp));
    assert.isOk(calculatorService.checkIfValidDate(validTimestampNumber));
    assert.isNotOk(calculatorService.checkIfValidDate(invalidTimestampString));
  });

  it("Should successfully validate current business day", () => {
    const currentTimestamp = "2019-10-07T12:01:30.000Z";
    const submitTimestamp = "2019-10-07T12:00:00.000Z";
    const validationResult = calculatorService.submitAtCurrentBusinessDay(
      currentTimestamp,
      submitTimestamp
    );
    assert.isOk(validationResult);
  });

  it("Should fail on validation for current business day", () => {
    const currentTimestamp = "2019-10-07T02:01:30.000Z";
    const submitTimestamp = "2019-10-07T02:00:00.000Z";
    const validationResult = calculatorService.submitAtCurrentBusinessDay(
      currentTimestamp,
      submitTimestamp
    );
    assert.isNotOk(validationResult);
  });

  it("Should count full business days and modulo of hours", () => {
    const halfOfBusinessDay = 4;
    const oneBusinessDay = 8;
    const oneAndHalfOfBusinessDay = 12;
    const fiveBusinessDaysAndHour = 41;

    const expectedHalfOfDay = {
      businessDays: 0,
      modulo: 4
    };
    const expectedOneDay = {
      businessDays: 1,
      modulo: 0
    };
    const expectedOneAndHalfDay = {
      businessDays: 1,
      modulo: 4
    };
    const expectedOneWeekAndHour = {
      businessDays: 5,
      modulo: 1
    };
    const resultHalfOfDay = calculatorService.calculateBusinessDays(
      halfOfBusinessDay
    );
    const resultOneDay = calculatorService.calculateBusinessDays(
      oneBusinessDay
    );
    const resultOneAndHalfOfDay = calculatorService.calculateBusinessDays(
      oneAndHalfOfBusinessDay
    );
    const resultOneWeekAndHour = calculatorService.calculateBusinessDays(
      fiveBusinessDaysAndHour
    );
    assert.deepEqual(expectedHalfOfDay, resultHalfOfDay);
    assert.deepEqual(expectedOneDay, resultOneDay);
    assert.deepEqual(expectedOneAndHalfDay, resultOneAndHalfOfDay);
    assert.deepEqual(expectedOneWeekAndHour, resultOneWeekAndHour);
  });
});
