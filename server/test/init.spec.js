const chai = require("chai");
const assert = chai.assert;

describe("Init tests", () => {
  it("Should have `chai` & `assert`", () => {
    assert.exists(chai, "chai dependency exists");
    assert.exists(assert, "assert dependency exists");
  });
});
